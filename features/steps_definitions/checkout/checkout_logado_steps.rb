Quando('inserir os dados de pagamento') do |dados_tabela|
  step 'selecionar endereço de realização do serviço'
  expect($ecomm_pages.checkout.btn_contratar['disabled']).to eql('true')
  $ecomm_pages.checkout.preencher_dados_cartao(data_load("cartao #{dados_tabela.rows_hash['bandeira']}"))
  expect($ecomm_pages.checkout.div_combo_parc).to be_truthy
  expect($ecomm_pages.checkout.btn_contratar['disabled']).to eql('false')
  $ecomm_pages.checkout.btn_contratar.click unless ENVIRONMENT_TYPE == 'prd'
end

Então('validar que o serviço foi comprado com sucesso') do
  if ENVIRONMENT_TYPE != 'prd'
    await(5) { $ecomm_pages.checkout.has_div_msg_sucess? }
    expect($ecomm_pages.checkout.div_msg_sucess).to be_truthy
    expect($ecomm_pages.checkout.div_msg_sucess.text).to eql('Serviço Contratado!')
    await(5) { !$ecomm_pages.checkout.div_num_order.nil? }
    expect($ecomm_pages.checkout.div_num_order).to be_truthy
  else
    print "\n***** AMBIENTE DE PRODUÇÃO !!! *****\n"
  end
end

E('realizar o login no checkout') do |dados_tabela|
  @email = data_load(dados_tabela.rows_hash['email'].to_s)
  @senha = data_load(dados_tabela.rows_hash['senha'].to_s)
  await(5) { $ecomm_pages.checkout.has_input_email? }
  expect($ecomm_pages.checkout.input_email).to be_truthy
  $ecomm_pages.checkout.input_email.set @email
  $ecomm_pages.checkout.input_senha.set @senha
  $ecomm_pages.checkout.btn_login.first.click
end

E('selecionar endereço de realização do serviço') do
  await(5) { $ecomm_pages.checkout.has_div_selec_endereco? }
  $ecomm_pages.checkout.div_selec_endereco.click
end

E('informar a placa do veículo') do
  await(5) { $ecomm_pages.checkout.has_input_placa_veiculo? }
  $ecomm_pages.checkout.input_placa_veiculo.set Faker::Vehicle.license_plate
end

E('informar os dados do usuário guest') do
  @cpf = gerador_cpf
  @email = gerador_email(@cpf)
  @nome = "#{Faker::Name.first_name} #{Faker::Name.last_name}"
  await(5) { $ecomm_pages.checkout.has_input_email? }
  $ecomm_pages.checkout.input_email.set @email
  await(1) { $ecomm_pages.checkout.has_input_senha? }
  $ecomm_pages.checkout.btn_login.first.click
  step 'informar os dados do usuário'
end

E('informar os dados do usuário') do
  binding.pry
  $ecomm_pages.checkout.input_nome_cartao.set @nome
  $ecomm_pages.checkout.input_cpf.set @cpf
  $ecomm_pages.checkout.input_telefone.set data_load(%w[telefones celular])
  expect($ecomm_pages.checkout.input_email['disabled']).to eql('true')
  expect($ecomm_pages.checkout.input_email.value).to eql(@email)
  $ecomm_pages.checkout.btn_login.first.click
end

E('informar o endereço de realização do serviço solicitado') do
  $ecomm_pages.checkout.preencher_endereco(data_load(%w[endereco tempotem cep]), data_load(%w[endereco tempotem numero]))
  $ecomm_pages.checkout.btn_login.first.click
  expect($ecomm_pages.checkout.wait_until_div_nao_sei_cep_invisible).to eql true
end
