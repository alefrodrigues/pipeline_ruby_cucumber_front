Quando('selecionar a categoria do serviço a ser contrato') do |dados_tabela|
  servico = dados_tabela.rows_hash['serviço']
  $ecomm_pages.home.selecionar_item_menu(dados_tabela.rows_hash['categoria'], servico)
  await(5) { $ecomm_pages.plp.has_div_categoria_servico? }
  expect($ecomm_pages.plp.div_categoria_servico.text).to include servico
end

E('selecionar o produto a ser contratado') do |dados_tabela|
  produto = dados_tabela.rows_hash['produto']
  expect($ecomm_pages.plp.div_categoria_servico).to be_truthy
  $ecomm_pages.plp.selecionar_produto(produto)
  expect($ecomm_pages.pdp.div_titulo_servico.visible?).to eql(true)
  expect($ecomm_pages.pdp.div_titulo_servico.text.downcase).to include produto.downcase
end

Quando('selecionar o agendamento do serviço') do
  await(5) { $ecomm_pages.pdp.has_div_btn_agendamento? }
  $ecomm_pages.pdp.div_btn_agendamento.click
  expect($ecomm_pages.pdp.div_modal_agendamento.visible?).to eql(true)
  $ecomm_pages.pdp.selecionar_data_agendamento
  $ecomm_pages.pdp.selecionar_horario_agendamento
  $ecomm_pages.pdp.responder_perguntas_detalhamento
end
