Dado('que acesso o site da tempo tem') do
  $ecomm_pages.home.load
end

E('acesso a tela de cadastro de cliente') do
  await(10) { $ecomm_pages.home.has_div_icon_perfil? }
  $ecomm_pages.home.div_icon_perfil.hover
  await(10) { $ecomm_pages.home.has_div_minha_conta? }
  $ecomm_pages.home.div_minha_conta.click
  $ecomm_pages.cadastro.acessar_cadastro
end

Quando('preencher os dados de cadastro') do |dados_tabela|
  @cpf = dados_tabela.rows_hash['cpf'].eql?('') ? gerador_cpf : dados_tabela.rows_hash['cpf']
  @email = dados_tabela.rows_hash['email'].eql?('') ? gerador_email(@cpf) : data_load(dados_tabela.rows_hash['email'])
  @senha = dados_tabela.rows_hash['senha'].eql?('') ? data_load(%w[password]) : dados_tabela.rows_hash['senha']
  @sobrenome = Faker::Name.last_name
  @nome = Faker::Name.first_name

  await(5) { $ecomm_pages.cadastro.has_cp_nome? }
  $ecomm_pages.cadastro.cp_nome.set @nome
  $ecomm_pages.cadastro.cp_sobrenome.set @sobrenome
  await(5) { $ecomm_pages.cadastro.has_cp_cpf? }
  $ecomm_pages.cadastro.cp_cpf.set @cpf
  await(5) { $ecomm_pages.cadastro.has_cp_email? }
  $ecomm_pages.cadastro.cp_email.set @email
  await(5) { $ecomm_pages.cadastro.has_cp_senha? }
  $ecomm_pages.cadastro.cp_senha.set @senha
  await(5) { $ecomm_pages.cadastro.has_cp_confSenha? }
  $ecomm_pages.cadastro.cp_confSenha.set @senha
  step 'clicar em finalizar cadastro'
end

Então('validar cadastro realizado com sucesso') do
  await(5) { $ecomm_pages.cadastro.has_msg_sucess? }
  expect($ecomm_pages.cadastro.msg_sucess).to be_truthy
end

Então('validar que o cadastro não foi realizado') do
  await(5) { $ecomm_pages.cadastro.has_msg_error? }
  expect($ecomm_pages.cadastro.msg_error).to be_truthy
end

Então('validar que o campo {string} não atende os requisitos') do |tipo|
  await(1) { $ecomm_pages.cadastro.has_no_btn_confCad? }
  $ecomm_pages.cadastro.finalizar_cadastro
  case tipo
  when 'email' then expect($ecomm_pages.cadastro.msg_campo_email_error).to be_truthy
  when 'senha' then expect($ecomm_pages.cadastro.msg_campo_senha_error).to be_truthy
  else
    raise "Tipo: #{tipo}, não disponível para validação !!!"
  end
end

Quando('clicar em finalizar cadastro') do
  await(5) { $ecomm_pages.cadastro.has_btn_confCad? }
  $ecomm_pages.cadastro.finalizar_cadastro
end

Então('validar os campos obrigatórios') do
  await(1) { $ecomm_pages.cadastro.has_msg_campo_nome_error? }
  step 'clicar em finalizar cadastro'
  expect($ecomm_pages.cadastro.msg_campo_nome_error).to be_visible
  expect($ecomm_pages.cadastro.msg_campo_sobrenome_error).to be_visible
  expect($ecomm_pages.cadastro.msg_campo_cpf_error).to be_visible
  expect($ecomm_pages.cadastro.msg_campo_email_error).to be_visible
  expect($ecomm_pages.cadastro.msg_campo_senha_error).to be_visible
  expect($ecomm_pages.cadastro.msg_campo_conf_senha_error).to be_visible
end
