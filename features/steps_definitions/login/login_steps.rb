Quando('informar os dados para realizar o login') do |dados_tabela|
  @email = data_load(dados_tabela.rows_hash['email'].to_s)
  @senha = data_load(dados_tabela.rows_hash['senha'].to_s)
  step 'acessar a tela de login'
  step 'realizar o login'
end

Dado('acessar a tela de login') do
  await(10) { $ecomm_pages.home.has_div_icon_perfil? }
  $ecomm_pages.home.div_icon_perfil.hover
  await(10) { $ecomm_pages.home.has_div_minha_conta? }
  $ecomm_pages.home.div_minha_conta.click
  await(5) { $ecomm_pages.login.has_cp_login? }
  await(5) { $ecomm_pages.login.has_cp_senha? }
end

Quando('informar os dados do login') do |dados_tabela|
  @email = data_load("login #{dados_tabela.rows_hash['fluxo']} login")
  @senha = data_load("login #{dados_tabela.rows_hash['fluxo']} senha")
  step 'acessar a tela de login'
  await(1) { $ecomm_pages.login.has_no_cp_login? }
  step 'realizar o login'
end

Então('validar exibição de obrigatóriedade no login {string}') do |validacao|
  expect($ecomm_pages.login.div_msg_pass_error).to be_truthy unless validacao.eql?('email')
  expect($ecomm_pages.login.div_msg_email_error).to be_truthy unless validacao.eql?('senha')
end

E('acessar a minha conta') do
  expect($ecomm_pages.minha_conta.div_info_contato.first.text).to include @nome, @sobrenome, @email
end

Dado('realizar o login') do
  $ecomm_pages.login.fazer_login(@email, @senha)
  $ecomm_pages.login.clicar_login
end

Então('validar que usuário esteja logado com sucesso') do
  expect($ecomm_pages.minha_conta.div_info_contato.first.text).to include @email
end

Então('validar que não é realizado o login') do
  expect($ecomm_pages.cadastro.msg_error).to be_truthy
end
