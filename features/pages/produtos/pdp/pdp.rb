module Ecomm
  module Produtos
    class Pdp < SitePrism::Page
      element :div_titulo_servico, 'div.product-description > div > div > h1'
      element :div_btn_agendamento, :xpath, '//*[@id="simple-button"]/div[text()="Agendar"]'
      element :div_btn_quero_ja, :xpath, '//*[@id="simple-button"]/div[text()="Quero já"]'
      element :div_modal_agendamento, 'div#m-content'
      element :div_btn_proximo, '#m-footer > div'
      # Detalhamento
      element :div_detalhamento, 'div#detalhamento'
      element :h3_pergunta_detalhamento, 'h3.question-title'
      elements :li_resp_detalhamento, 'ul.container-choices > li'
      elements :lst_produtos, '.product-image-wrapper > img'
      elements :span_data_agendamento, 'span.datetime__picker__month-day'
      elements :span_hr_agendamento, 'span.datetime__picker__clock-time'

      def selecionar_data_agendamento
        await(5) { has_span_data_agendamento? }
        span_data_agendamento[2].click
        span_data_agendamento[2].text
      end

      def selecionar_horario_agendamento
        await(5) { has_span_hr_agendamento? }
        span_hr_agendamento[2].click
        span_hr_agendamento[2].text
      end

      def responder_perguntas_detalhamento
        perguntas = {}
        wait_until_div_modal_agendamento_invisible wait: 15
        while has_div_detalhamento?
          pergunta = h3_pergunta_detalhamento.text
          resposta = li_resp_detalhamento.sample.click
          perguntas["P: #{pergunta}"] = "R: #{resposta.text}"
          await(1) { has_no_h3_pergunta_detalhamento? }
        end
        perguntas.empty? ? (print "\n\nNão foi solicitado nenhum tipo de detalhamento ao contratante !\n\n") : perguntas
      end
    end
  end
end
