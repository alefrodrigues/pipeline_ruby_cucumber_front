module Ecomm
  module Produtos
    class Plp < SitePrism::Page
      set_url '/'

      element :div_categoria_servico, '.page-title-wrapper'
      elements :lst_produtos, '.product-item-link'

      def selecionar_produto(produto)
        await(5) { has_lst_produtos? }
        lst_produtos.each do |prod|
          break prod.click if prod.text.downcase.include? produto.downcase
        end
      end
    end
  end
end
