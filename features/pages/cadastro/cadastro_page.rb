module Ecomm
  module Cadastro
    class Cadastro < SitePrism::Page
      element :btn_cad, 'a[class*=create]'
      element :cp_nome, 'div[class=control]>input[id=firstname]'
      element :cp_sobrenome, 'div[class=control]>input[id=lastname]'
      element :cp_cpf, 'div[class=control]>input[id=taxvat]'
      element :cp_email, 'div[class=control]>input[id=email_address]'
      element :cp_senha, 'div[class=control]>input[id=password]'
      element :cp_confSenha, 'div[class=control]>input[id=password-confirmation]'
      element :btn_confCad, 'div[class=primary]>button[class*=action]'
      element :msg_sucess, 'div[class*=message-success]'
      element :msg_error, 'div[data-ui-id=message-error]'
      element :msg_campo_email_error, '#email_address-error'
      element :msg_campo_senha_error, '#password-error'
      element :msg_campo_nome_error, '#firstname-error'
      element :msg_campo_sobrenome_error, '#lastname-error'
      element :msg_campo_cpf_error, '#taxvat-error'
      element :msg_campo_conf_senha_error, '#password-confirmation-error'

      def acessar_cadastro
        btn_cad.click
      end

      def finalizar_cadastro
        btn_confCad.click
      end
    end
  end
end
