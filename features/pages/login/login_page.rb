module Ecomm
  module Login
    class Login < SitePrism::Page
      element :cp_login, '#email'
      element :cp_senha, '#pass'
      element :btn_login, '#send2'
      element :div_msg_email_error, '#email-error'
      element :div_msg_pass_error, '#pass-error'

      def fazer_login(user, pass)
        cp_login.set user
        cp_senha.set pass
      end

      def clicar_login
        btn_login.click
      end
    end
  end
end
