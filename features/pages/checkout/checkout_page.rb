module Ecomm
  module Checkout
    class Checkout < SitePrism::Page
      element :input_email, '#ipt-e-mail'
      element :input_senha, '#ipt-senha'
      element :input_cpf, '#ipt-cpf'
      element :input_telefone, '#ipt-telefone'
      element :div_selec_endereco, '.adc-address > p'
      element :div_msg_sucess, 'div[id=success-header] > h2'
      element :div_num_order, 'div[id=order-number] > p'
      element :div_erro_aut_cartao, '.notificationBody error'
      # Placa
      element :input_placa_veiculo, 'input[id="ipt-placado veículo"]'
      # Endereco
      element :div_add_novo_endereco, 'div#add-address'
      element :input_cep, '#ipt-cep'
      element :input_numero_residencial, '#ipt-número'
      element :input_endereco, '#ipt-endereço'
      element :div_nao_sei_cep, '#srcCep'
      # Cartao
      element :div_add_cartao, 'div#add-card'
      element :input_nome_cartao, '#ipt-nome'
      element :input_num_cartao, 'input[id=encryptedCardNumber]'
      element :input_valid_cartao, 'input[class="js-iframe-input date-field input-field"]'
      element :input_cvv_cartao, 'input[class="js-iframe-input cvc-field input-field"]'
      element :div_combo_parc, 'div[class="ipt-select filled"]'
      element :btn_contratar, 'button[tt-button="primary"]'
      elements :btn_login, 'div.filled'

      def preencher_endereco(cep, numero)
        await(5) { has_input_cep? }
        input_cep.set cep
        await(5) { input_endereco }
        input_numero_residencial.set numero
      end

      def preencher_dados_cartao(cartao)
        await(5) { has_div_add_cartao? }
        div_add_cartao.click
        await(5) { has_input_nome_cartao? }
        input_nome_cartao.set cartao['nome']
        input_cartao_dados(cartao['numero'], cartao['validade'], cartao['cvv'])
      end

      def input_cartao_dados(numero, valid, cvv)
        within_frame(:xpath, '//*[@id="card-container"]/div/div/div[2]/div/div[1]/label/span[2]/span/iframe', wait: 5) do
          input_num_cartao.set(numero)
        end

        within_frame(:xpath, '//*[@id="card-container"]/div/div/div[2]/div/div[2]/div[1]/label/span[2]/span/iframe', wait: 5) do
          input_valid_cartao.set(valid)
        end

        within_frame(:xpath, '//*[@id="card-container"]/div/div/div[2]/div/div[2]/div[2]/label/span[2]/span/iframe', wait: 5) do
          input_cvv_cartao.set(cvv)
        end
      end
    end
  end
end
