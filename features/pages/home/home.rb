module Ecomm
  module Home
    class Home < SitePrism::Page
      set_url '/'

      element :div_icon_perfil, 'div[id=login]'
      element :div_minha_conta, 'div[class=floating-content]'
    end
  end
end
