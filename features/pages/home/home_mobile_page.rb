module Ecomm
  module Home
    class HomeMobile < Ecomm::Home::Home
      set_url '/'

      element :ul_nav_menu, 'div[id=burguer]'
      elements :lst_opc_menu, 'li[class="sb-items dropdown"] > span'
      elements :lst_itens_menu, 'li[class="sb-items dropdown opened"] > div.dropdown-list > ul > a > li > span'

      def selecionar_item_menu(opcao_menu, item_menu)
        selecionar_opcao_menu(opcao_menu)
        await(5) { has_lst_itens_menu? }
        lst_itens_menu.each do |item|
          break item.click if item.text.upcase.eql?(item_menu.upcase)
        end
      end

      private

      def selecionar_opcao_menu(opcao_menu)
        await(1) { has_no_ul_nav_menu? }
        ul_nav_menu.click
        await(1) { has_lst_opc_menu? }
        lst_opc_menu.each do |opcao|
          break opcao.click if opcao.text.upcase.include? opcao_menu.upcase
        end
      end
    end
  end
end
