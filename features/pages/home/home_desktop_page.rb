module Ecomm
  module Home
    class HomeDesktop < Ecomm::Home::Home
      set_url '/'

      element :ul_nav_menu, '#ui-id-1'
      elements :lst_opc_menu, 'li[class="nav-items dropdown"] > span'
      elements :lst_itens_menu, 'li[class="dropdown-items"] > span'

      def selecionar_item_menu(opcao_menu, item_menu)
        selecionar_opcao_menu(opcao_menu)
        await(5) { has_lst_itens_menu? }
        lst_itens_menu.each do |item|
          break item.click if item.text.upcase.eql?(item_menu.upcase)
        end
      end

      private

      def selecionar_opcao_menu(opcao_menu)
        await(1) { has_no_lst_opc_menu? }
        lst_opc_menu.each do |opcao|
          break opcao.hover if opcao.text.upcase.include? opcao_menu.upcase
        end
      end
    end
  end
end
