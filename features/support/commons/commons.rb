module StoreCommons
  def await(timeout = Capybara.default_wait_time)
    timeout.times do
      break if yield

      sleep 1
    end
  rescue Selenium::WebDriver::Error::StaleElementReferenceError, StandardError => exception
    puts 'Elemento avaliado não está mais na tela'
    puts exception
  rescue StandardError => exception
    raise exception
  end

  def reload_screen
    Capybara.current_session.driver.browser.navigate.refresh
  end

  def highlight(selector)
    page.execute_script("$('#{selector}').css({'border': '3px solid #cc008f'});")
  end

  def highlight_element(elemento)
    highlight elemento.instance_variable_get(:@query).locator
  end

  def scroll_custom(elemento)
    selector = elemento.instance_variable_get(:@query).locator
    page.execute_script("const container1 = document.querySelector('#{selector}'); container1.scrollTop = 1000;")
  end

  def camel_case_lower(string)
    string.split('_').inject([]) { |buffer, e| buffer.push(buffer.empty? ? e : e.capitalize) }.join
  end

  def data_load(keys)
    # busca massa no arquivo por ambiente
    data = search(keys, ECOMM_DATA)
    return data unless data.nil?

    # busca massa no arquivo geral
    search(keys, ECOMM_GERAL)
  end

  def page_load(page)
    page.load
    expect(page).to be_displayed
  end

  def validate_browser_open
    browser_open { current_url.eql? 'data:,' }
  end

  def gerador_cpf
    @cpf = Faker::CPF.numeric
  end

  def gerador_email(info)
    @email = "test_tempotem#{info}@mailsac.com"
  end

  private

  def browser_open(limit = 5, current = 1, &block)
    yield
  rescue StandardError => exception
    raise exception if current > limit

    puts "------ tentativa #{current}/#{limit} de abrir navegador falhou -------\n #{exception}"
    sleep 5
    browser_open(limit, current + 1, &block)
  end

  def current_url
    Capybara.current_session.driver.browser.current_url
  end

  def search(keys, data)
    keys = keys.split(' ') if keys.instance_of? String
    keys.each do |key|
      data = data[key]
      break if data.nil?
    end
    data
  end
end
