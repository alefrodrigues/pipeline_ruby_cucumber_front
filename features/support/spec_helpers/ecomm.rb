Dir[File.join(File.dirname(__FILE__), 'pages/*.rb')].sort.each { |file| require file }

module Ecomm
  class Pages
    def home
      MOBILE ? Ecomm::Home::HomeMobile.new : Ecomm::Home::HomeDesktop.new
    end

    def cadastro
      Ecomm::Cadastro::Cadastro.new
    end

    def login
      Ecomm::Login::Login.new
    end

    def minha_conta
      Ecomm::MinhaConta::MinhaConta.new
    end

    def plp
      Ecomm::Produtos::Plp.new
    end

    def pdp
      MOBILE ? Ecomm::Produtos::PdpMobile.new : Ecomm::Produtos::PdpDesktop.new
    end

    def checkout
      Ecomm::Checkout::Checkout.new
    end
  end
end
