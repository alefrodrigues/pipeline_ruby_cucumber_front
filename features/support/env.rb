require 'capybara'
require 'capybara/cucumber'
require 'rspec'
require 'selenium-webdriver'
require 'site_prism'
require 'pry'
require 'faker'
require 'cpf_faker'

Dir[File.join(File.dirname(__FILE__), 'commons/*.rb')].sort.each { |file| require_relative file }

BROWSER = ENV['BROWSER']
ENVIRONMENT_TYPE ||= ENV['ENVIRONMENT_TYPE']

def load_yaml_file(path)
  YAML.load_file(File.dirname(__FILE__) + path)
end

ECOMM_CONFIG = load_yaml_file("/config/#{ENVIRONMENT_TYPE}.yaml")
ECOMM_DATA = load_yaml_file("/fixtures/#{ENVIRONMENT_TYPE}.yaml")
ECOMM_GERAL = load_yaml_file('/fixtures/geral.yaml')

HEADLESS = ENV['HEADLESS'] ? true : false
NOT_LOAD_IMAGES = ENV['NOT_LOAD_IMAGES'] ? true : false

if ENV['RESOLUTION']
  dimensions = ENV['RESOLUTION'].split('x')
  WIDTH = dimensions.first
  HEIGHT = dimensions.last
else
  WIDTH = ENV['WIDTH']
  HEIGHT = ENV['HEIGHT']
end

Capybara.register_driver :selenium do |app|
  if BROWSER.eql?('chrome')
    MOBILE = false
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: chrome_options, desired_capabilities: chrome_capabilities(ENV['PAGE_LOAD_STRATEGY']))
  elsif BROWSER.eql?('mobile')
    MOBILE = true
    DEVICE = ENV['DEVICE'].dup.tr('_', ' ') if ENV['DEVICE']
    mobile_emulation = { device_name: DEVICE }
    puts 'Device: ' + DEVICE
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: chrome_options(mobile_emulation), desired_capabilities: chrome_capabilities(ENV['PAGE_LOAD_STRATEGY']))
  elsif BROWSER.eql?('firefox')
    MOBILE = false
    Capybara::Selenium::Driver.new(app, browser: :firefox, marionette: true, desired_capabilities: firefox_capabilities(ENV['PAGE_LOAD_STRATEGY']))
  elsif BROWSER.eql?('internet_explorer')
    MOBILE = false
    Capybara::Selenium::Driver.new(app, browser: :ie)
  elsif BROWSER.eql?('edge')
    MOBILE = false
    Capybara::Selenium::Driver.new(app, browser: :edge)
  elsif BROWSER.eql?('safari')
    MOBILE = false
    Capybara::Selenium::Driver.new(app, browser: :safari)
  end
end

def chrome_options(mobile_emulation = nil)
  options = Selenium::WebDriver::Chrome::Options.new
  if mobile_emulation
    if WIDTH && HEIGHT
      user_agent = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Mobile Safari/537.36'
      mobile_emulation = { device_metrics: { width: WIDTH.to_i, height: HEIGHT.to_i, pixelRatio: 1, touch: true }, user_agent: user_agent }
    end
    options.add_emulation(mobile_emulation)
  end

  if HEADLESS
    options.add_argument('--headless')
    options.add_argument('disable-gpu')
  end

  options.add_argument('--no-sandbox')
  options.add_argument('ignore-certificate-errors')
  options.add_argument('disable-popup-blocking')
  options.add_argument('disable-translate')
  options.add_argument('disable-infobars')
  options.add_argument('--enable-features=NetworkService,NetworkServiceInProcess')
  options.add_argument('--disable-features=VizDisplayCompositor')
  options.add_argument('--disable-dev-shm-usage')
  options.add_preference('download.default_directory', File.absolute_path('./downloads'))
  options.add_preference('download.prompt_for_download', false)
  options.add_preference('profile.managed_default_content_settings.images', 2) if NOT_LOAD_IMAGES
  options
end

def chrome_capabilities(page_load_strategy)
  page_load_strategy ||= 'normal'
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(page_load_strategy: page_load_strategy.downcase)
  capabilities
end

def firefox_capabilities(page_load_strategy)
  page_load_strategy ||= 'normal'
  capabilities = Selenium::WebDriver::Remote::Capabilities.firefox
  capabilities['pageLoadStrategy'] = page_load_strategy.downcase
  capabilities
end

Capybara.configure do |config|
  config.default_driver = :selenium
  config.app_host = ECOMM_CONFIG['url_login']
end

Capybara.default_max_wait_time = 10
