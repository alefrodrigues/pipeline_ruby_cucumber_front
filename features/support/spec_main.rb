Dir[File.join(File.dirname(__FILE__), 'spec_helpers/*.rb')].sort.each { |file| require file }

$ecomm_pages = Ecomm::Pages.new

World(StoreCommons)

SitePrism::Page.class_eval do
  include StoreCommons
end

SitePrism::Section.class_eval do
  include StoreCommons
end
