Before do
  validate_browser_open

  largura = WIDTH
  largura ||= MOBILE ? 320 : 1366

  altura = HEIGHT
  altura ||= MOBILE ? 730 : 768

  target_size = Selenium::WebDriver::Dimension.new(largura.to_i, altura.to_i)
  Capybara.current_session.driver.browser.manage.window.size = target_size
end

After do |scenario|
  add_screenshot(scenario)
  Capybara.current_session.driver.quit
end

def add_screenshot(scenario)
  nome_cenario = scenario.name.gsub(/[^A-Za-z0-9]/, '')
  nome_cenario = nome_cenario.gsub(' ', '_').downcase!
  screen = scenario.failed? ? "log/screenshots/evidencias_negativas/#{nome_cenario}.png" : "log/screenshots/evidencias_positivas/#{nome_cenario}.png"
  page.save_screenshot(screen)
  embed(screen, 'image/png', 'Clique aqui: ')
end
