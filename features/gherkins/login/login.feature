#language :pt

@%login
Funcionalidade: Login no site da Tempo Tem
  Sendo um usuário no site
  Posso informar os dados nos campos de Login
  Para acessar a Minha Conta

@login_sucesso
Cenario: Login: Logar com sucesso
  Dado que acesso o site da tempo tem
  Quando informar os dados para realizar o login
    | email | email_cadastrado |
    | senha | password         |
  Então validar que usuário esteja logado com sucesso

@login_negativas_campos_obrigatorios
Esquema do Cenario: Login: Validação negativas de Login campos obrigatórios
  Dado que acesso o site da tempo tem
  Quando informar os dados do login
    | fluxo | <fluxos> |
  Então validar exibição de obrigatóriedade no login "<validacao>"
    Exemplos:
      | fluxos                | validacao |
      | login_sem_email_senha | ambos     |
      | login_sem_email       | email     |
      | login_sem_senha       | senha     |
      | login_invalido        | email     |

@login_negativas_senha_incorreta
Cenário: Login: Validação negativas de Login Senha Incorreta
  Dado que acesso o site da tempo tem
  Quando informar os dados do login
    | fluxo | login_senha_errada |
  Então validar que não é realizado o login
