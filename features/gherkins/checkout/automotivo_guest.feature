#language :pt

@%servico_automotivo
@%servico_automotivo_usuario_guest
@%checkout_usuario_guest
Funcionalidade: Realizar Checkout no site TempoTem - Guest
  Sendo um usuário visitante no site
  Posso selecionar determinado serviço
  Para realizar o agendamento/compra do serviço sem se cadastrar no site.

@ar_condicionado
Cenário: Realizar compra de Serviço Residencial - Ar Condicionado
  Dado que acesso o site da tempo tem
  E selecionar a categoria do serviço a ser contrato
    | categoria | Residenciais    |
    | serviço   | Ar Condicionado |
  Quando selecionar o produto a ser contratado
    | produto | Higienização de Ar Condicionado |
  E selecionar o agendamento do serviço
  E informar os dados do usuário guest
  E informar o endereço de realização do serviço solicitado
  E inserir os dados de pagamento
    | bandeira | elo |
  Então validar que o serviço foi comprado com sucesso
