#language :pt

@%servico_automotivo
@%servico_automotivo_usuario_cadastrado
@%checkout_usuario_cadastrado
Funcionalidade: Realizar Checkout no site TempoTem - Logado
  Sendo um usuário no site
  Posso selecionar determinado serviço
  Para realizar o agendamento/compra do serviço automotivos

@servico_automotivo_guincho_25km
Cenário: Realizar compra de Serviço Automotivo - Guincho 25KM
  Dado que acesso o site da tempo tem
  E selecionar a categoria do serviço a ser contrato
    | categoria | Carro   |
    | serviço   | Guincho |
  Quando selecionar o produto a ser contratado
    | produto | Guincho até 25km |
  E selecionar o agendamento do serviço
  E realizar o login no checkout
    | email | checkout_automotivo |
    | senha | password            |
  E informar a placa do veículo
  E inserir os dados de pagamento
    | bandeira | mastercard |
  Então validar que o serviço foi comprado com sucesso