#language :pt

@%servico_residencial
@%servico_residencial_usuario_cadastrado
@%checkout_usuario_cadastrado
Funcionalidade: Realizar Checkout no site TempoTem - Logado
  Sendo um usuário no site
  Posso selecionar determinado serviço
  Para realizar o agendamento/compra do serviço

@ar_condicionado
Cenário: Realizar compra de Serviço Residencial - Ar Condicionado
  Dado que acesso o site da tempo tem
  E selecionar a categoria do serviço a ser contrato
    | categoria | Residenciais    |
    | serviço   | Ar Condicionado |
  Quando selecionar o produto a ser contratado
    | produto | Higienização de Ar Condicionado |
  E selecionar o agendamento do serviço
  E realizar o login no checkout
    | email | checkout_residencial |
    | senha | password             |
  E inserir os dados de pagamento
    | bandeira | visa |
  Então validar que o serviço foi comprado com sucesso

@chaveiro_residencial
Cenário: Realizar compra de Serviço Residencial - Chaveiro Residencial
  Dado que acesso o site da tempo tem
  E selecionar a categoria do serviço a ser contrato
    | categoria | Residenciais         |
    | serviço   | Chaveiro Residencial |
  Quando selecionar o produto a ser contratado
    | produto | Troca de Trava Tetra |
  E selecionar o agendamento do serviço
  E realizar o login no checkout
    | email | checkout_residencial |
    | senha | password             |
  E inserir os dados de pagamento
    | bandeira | visa |
  Então validar que o serviço foi comprado com sucesso

@eletricista
Cenário: Realizar compra de Serviço Residencial - Troca de Tomadas
  Dado que acesso o site da tempo tem
  E selecionar a categoria do serviço a ser contrato
    | categoria | Residenciais |
    | serviço   | Eletricista  |
  Quando selecionar o produto a ser contratado
    | produto | Troca de tomadas |
  E selecionar o agendamento do serviço
  E realizar o login no checkout
    | email | checkout_residencial |
    | senha | password             |
  E inserir os dados de pagamento
    | bandeira | mastercard |
  Então validar que o serviço foi comprado com sucesso
