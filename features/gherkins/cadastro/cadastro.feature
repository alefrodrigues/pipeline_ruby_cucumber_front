#language :pt

@%cadastro
Funcionalidade: Cadastro no site da Tempo Tem
  Sendo um usuário no site
  Posso acessar o módulo de cadastro do site
  Para cadastrar os meus dados para agendamento

@realizar_cadastro_cliente
Cenário: Cadastro: Realizar cadastro com sucesso
  Dado que acesso o site da tempo tem
  E acesso a tela de cadastro de cliente
  Quando preencher os dados de cadastro
    | email | |
    | cpf   | |
    | senha | |
  Então validar cadastro realizado com sucesso
  E acessar a minha conta

@cliente_com_email_cadastrado
Cenário: Cadastro: Tentativa de Cadastro com e-mail já cadastrado
  Dado que acesso o site da tempo tem
  E acesso a tela de cadastro de cliente
  Quando preencher os dados de cadastro
    | email | email_cadastrado |
    | cpf   |                  |
    | senha |                  |
  Então validar que o cadastro não foi realizado

@cadastro_validacao_campo_email
Esquema do Cenário: Cadastro: Validação campo de e-mail
Dado que acesso o site da tempo tem
E acesso a tela de cadastro de cliente
Quando preencher os dados de cadastro
  | email | <email> |
  | cpf   |         |
  | senha |         |
Então validar que o campo 'email' não atende os requisitos
  Exemplos:
    | email            |
    | 2132154@         |
    | emailtest.com.br |
    | emailtest        |

@cadastro_validacao_campo_senha_formulario
Esquema do Cenário: Cadastro: Validação campo de Senha
  Dado que acesso o site da tempo tem
  E acesso a tela de cadastro de cliente
  Quando preencher os dados de cadastro
    | email |         |
    | cpf   |         |
    | senha | <senha> |
  Então validar que o campo 'senha' não atende os requisitos
    Exemplos:
      | senha    |
      | 12345678 |

@cadastro_validacao_campo_senha
Esquema do Cenário: Cadastro: Validação campo de Senha
  Dado que acesso o site da tempo tem
  E acesso a tela de cadastro de cliente
  Quando preencher os dados de cadastro
    | email |         |
    | cpf   |         |
    | senha | <senha> |
  Então validar que o cadastro não foi realizado
    Exemplos:
      | senha    |
      | @1234567 |
      | senhanum |
      | senha12  |
      | $%%¨¨&** |
      | 1236     |

@cliente_validacao_formulario
Cenário: Cadastro: Validação Campos obrigatórios no Cadastro
  Dado que acesso o site da tempo tem
  E acesso a tela de cadastro de cliente
  Quando clicar em finalizar cadastro
  Então validar os campos obrigatórios