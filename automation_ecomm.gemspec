lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'automation-ecomm'
  spec.version       = '1.0.0'
  spec.authors       = ['Alef Rodrigues']
  spec.email         = ['alef.rodrigues@groove.tech']

  spec.summary       = 'Testes de FrontEnd'
  spec.description   = 'Automação de testes de FrontEnd.'
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'capybara'
  spec.add_dependency 'cpf_faker'
  spec.add_dependency 'cucumber'
  spec.add_dependency 'dbi'
  spec.add_dependency 'faker'
  spec.add_dependency 'mime-types'
  spec.add_dependency 'os'
  spec.add_dependency 'parallel_tests'
  spec.add_dependency 'rspec'
  spec.add_dependency 'selenium-webdriver'
  spec.add_dependency 'site_prism'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-checkstyle_formatter'

  # Windows
  spec.add_dependency 'ffi' if Gem.win_platform?
end
